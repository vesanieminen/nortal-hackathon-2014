﻿using UnityEngine;
using System.Collections;

public class UI : MonoBehaviour {

    public GameObject grid;
    public GameObject wall;
    public GameObject floor;
    public Transform imageTarget;
    public Transform props;
    float bottomLimit;
    float topLimit;
    float leftLimit;
    float rightLimit;
    float limitMargin = 0.1f;

    float componentMargin = 10;
    float nextComponentY;
    float componentHeightOrigin = 30;
	float componentWidthOrigin = 50;
	float componentHeight;
	float componentWidth;
	float vSliderValue = 0.5F;
    public GameObject light;

    int layerMask = 1 << 8;

    Object[] textures;
    int textureIndex;

    enum WallSide{
        Left,
        Right,
        Top,
        Bottom
    }

    void Start() {
        bottomLimit = imageTarget.position.y - imageTarget.lossyScale.y / 2 - limitMargin;

		if (iPhone.generation == iPhoneGeneration.iPhone4S) {
			componentHeight = componentHeightOrigin * 4;
			componentWidth = componentWidthOrigin * 3;
            GetComponent<ColorPicker>().sizeFull = GetComponent<ColorPicker>().sizeFull * 4;
            GetComponent<ColorPicker>().sizeHidden = GetComponent<ColorPicker>().sizeHidden * 4;
		}
		else if (iPhone.generation == iPhoneGeneration.iPad3Gen) {
			componentHeight = componentHeightOrigin * 4;
			componentWidth = componentWidthOrigin * 4;
            GetComponent<ColorPicker>().sizeFull = GetComponent<ColorPicker>().sizeFull * 4;
            GetComponent<ColorPicker>().sizeHidden = GetComponent<ColorPicker>().sizeHidden * 4;
		}
        else {
			componentHeight = componentHeightOrigin;
			componentWidth = componentWidthOrigin;
        }
        CreateWallAndFloorMesh();

        textures = Resources.LoadAll("Textures");
        Debug.Log(textures.Length);
    }

    const int xStartPos = 10;
    void OnGUI() {
        GUI.skin.verticalSlider.stretchWidth = true;
        GUI.skin.verticalSliderThumb.stretchWidth = true;
        nextComponentY = componentMargin;
        if (GUI.Button(new Rect(xStartPos, nextComponentY, componentWidth, componentHeight), "Grid")) {
            grid.active = !grid.active;
        }
        nextComponentY += componentHeight + componentMargin;
        if (GUI.Button(new Rect(xStartPos, nextComponentY, componentWidth, componentHeight), "Wall")) {
            wall.active = !wall.active;
        }
        nextComponentY += componentHeight + componentMargin;
        if (GUI.Button(new Rect(xStartPos, nextComponentY, componentWidth + 10, componentHeight), "Texture")) {
            ChangeTexture();
        }
        nextComponentY += componentHeight + componentMargin;
        if (GUI.Button(new Rect(xStartPos, nextComponentY, componentWidth, componentHeight), "Props")) {
            props.active = !props.active;
        }
        nextComponentY += componentHeight + componentMargin;
        GetComponent<ColorPicker>().startPos = new Vector2(xStartPos * 2, nextComponentY);
		nextComponentY += componentHeight + componentMargin;

		vSliderValue = GUI.VerticalSlider (new Rect(Screen.width - xStartPos * 3, componentMargin, componentWidth, componentHeight * 3), vSliderValue, 1F, 0.0F);
		light.GetComponent<Light>().intensity = vSliderValue;

		Color c = wall.renderer.sharedMaterial.color;
		float val = GUI.VerticalSlider (new Rect(Screen.width - xStartPos * 3, componentMargin * 2 + componentHeight * 3, componentWidth, componentHeight * 3), c.a, 1F, 0.0F);
		wall.renderer.sharedMaterial.color = new Color (c.r, c.g, c.b, val);
		Color c2 = floor.renderer.sharedMaterial.color;
		floor.renderer.sharedMaterial.color = new Color (c2.r, c2.g, c2.b, val);
    }

    void ChangeTexture() {
        wall.renderer.material.mainTexture = textures[++textureIndex % textures.Length] as Texture2D;
    }

    void Update() {
        var picker = GetComponent<ColorPicker>();
        if (picker.mState == ColorPicker.ESTATE.Hidden) {
            if (Input.GetButton("Fire1") &&
                Input.mousePosition.x > componentWidth &&
                Input.mousePosition.x < Screen.width - componentWidth
            ) {
                RaycastHit hit;
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Debug.DrawLine(ray.origin, ray.origin + ray.direction, Color.red);
                if (Physics.Raycast(ray.origin, ray.direction, out hit, Mathf.Infinity, layerMask)) {
                    var start = new Vector3(hit.point.x -10, hit.point.y, hit.point.z);
                    var end = new Vector3(hit.point.x + 10, hit.point.y, hit.point.z);
                    Debug.DrawLine(start, end, Color.green);
                    if (hit.point.y < bottomLimit) {
                        UpdateWallAndFloorMesh(WallSide.Bottom, hit.point.y);
                    }
                    else if (hit.point.y > topLimit) {
                        UpdateWallAndFloorMesh(WallSide.Top, hit.point.y);
                    }
                    else if (hit.point.x < leftLimit) {
                        UpdateWallAndFloorMesh(WallSide.Left, hit.point.x);
                    }
                    else if (hit.point.x > rightLimit) {
                        UpdateWallAndFloorMesh(WallSide.Right, hit.point.x);
                    }
                }
            }
        }
    }

    void UpdateWallAndFloorMesh(WallSide side, float val) {
        var wallMesh = wall.GetComponent<MeshFilter>().mesh;
        var wallVertices = wallMesh.vertices;
        Vector3 v1,v2 = Vector3.zero;

        var floorMesh = floor.GetComponent<MeshFilter>().mesh;
        var floorVertices = floorMesh.vertices;
        Vector3 v3,v4,v5,v6 = Vector3.zero;
        switch (side) {
            case WallSide.Bottom:
                v1 = wallVertices[2];
                v1.y = val;
                wallVertices[2] = v1;
                v2 = wallVertices[3];
                v2.y = val;
                wallVertices[3] = v2;

                // floor vertices
                v3 = floorVertices[0];
                v3.y = val;
                floorVertices[0] = v3;
                v4 = floorVertices[1];
                v4.y = val;
                floorVertices[1] = v4;
                v5 = floorVertices[2];
                v5.y = val;
                floorVertices[2] = v5;
                v6 = floorVertices[3];
                v6.y = val;
                floorVertices[3] = v6;
            break;
            case WallSide.Top:
                v1 = wallVertices[0];
                v1.y = val;
                wallVertices[0] = v1;
                v2 = wallVertices[1];
                v2.y = val;
                wallVertices[1] = v2;
            break;
            case WallSide.Left:
                v1 = wallVertices[0];
                v1.x = val;
                wallVertices[0] = v1;
                v2 = wallVertices[3];
                v2.x = val;
                wallVertices[3] = v2;
                // floor vertices
                v3 = floorVertices[0];
                v3.x = val;
                floorVertices[0] = v3;
                v4 = floorVertices[3];
                v4.x = val;
                floorVertices[3] = v4;
            break;
            case WallSide.Right:
                v1 = wallVertices[1];
                v1.x = val;
                wallVertices[1] = v1;
                v2 = wallVertices[2];
                v2.x = val;
                wallVertices[2] = v2;
                // floor vertices
                v3 = floorVertices[1];
                v3.x = val;
                floorVertices[1] = v3;
                v4 = floorVertices[2];
                v4.x = val;
                floorVertices[2] = v4;
            break;
        }
        wallMesh.vertices = wallVertices;
        wallMesh.RecalculateNormals();
        wallMesh.RecalculateBounds();
        wall.GetComponent<MeshCollider>().mesh = wallMesh;
        var height = Mathf.Abs(wallVertices[0].y - wallVertices[2].y);
        var width = Mathf.Abs(wallVertices[0].x - wallVertices[2].x);
        wall.renderer.sharedMaterial.mainTextureScale = new Vector2(width, height);
        wall.renderer.sharedMaterial.SetTextureScale("_BumpMap", new Vector2(width, height));

        floorMesh.vertices = floorVertices;
        floorMesh.RecalculateNormals();
        floorMesh.RecalculateBounds();
        floor.GetComponent<MeshCollider>().mesh = floorMesh;
        var floorHeight = Mathf.Abs(floorVertices[0].z - floorVertices[2].z);
        var floorWidth = Mathf.Abs(floorVertices[0].x - floorVertices[2].x);
        floor.renderer.sharedMaterial.mainTextureScale = new Vector2(floorWidth, floorHeight);

        var pos = props.position;
        pos.y = floorVertices[0].y;
        props.position = pos;
    }

    void CreateWallAndFloorMesh() {
        float width = 5;
        float height = 2.5f;
        Vector3[] vertices = {
            new Vector3(-width/2, height, 0),
            new Vector3(width/2, height, 0),
            new Vector3(width/2, -height, 0),
            new Vector3(-width/2, -height, 0)
        };
        int[] triangles = {0,1,2,0,2,3};
        Vector2[] uv = {new Vector2(0,0),new Vector2(1,0), new Vector2(1,1), new Vector2(0,1)};
        var mesh = wall.GetComponent<MeshFilter>().mesh;
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uv;
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        wall.GetComponent<MeshCollider>().mesh = mesh;
        wall.renderer.sharedMaterial.mainTextureScale = new Vector2(width, height);
        wall.renderer.sharedMaterial.SetTextureScale("_BumpMap", new Vector2(width, height));

        Vector3[] floorVertices = {
            new Vector3(-width/2, -height, 0),
            new Vector3(width/2, -height, 0),
            new Vector3(width/2, -height, -height * 2),
            new Vector3(-width/2, -height, -height * 2)
        };
        int[] floorTriangles = {0,1,2,0,2,3};
        Vector2[] floorUV = {new Vector2(0,0),new Vector2(1,0), new Vector2(1,1), new Vector2(0,1)};
        var floorMesh = floor.GetComponent<MeshFilter>().mesh;
        floorMesh.Clear();
        floorMesh.vertices = floorVertices;
        floorMesh.triangles = floorTriangles;
        floorMesh.uv = floorUV;
        floorMesh.RecalculateNormals();
        floorMesh.RecalculateBounds();
        floor.GetComponent<MeshCollider>().mesh = floorMesh;
        floor.renderer.sharedMaterial.mainTextureScale = new Vector2(width, height);

        var pos = props.position;
        pos.y = -height;
        props.position = pos;
    }

}
