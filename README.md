# AR Designer #

This is a demo of a tablet/smart phone app that allows the user to place furniture in a room and change the colours or textures of the walls or floor.

We're using Qualcomm's free AR library called [Vuforia](https://developer.vuforia.com) for the image tracking.

The demo uses the [Nortal Hackathon 2014 poster](https://www.flickr.com/photos/vesanieminen/15362990600/in/set-72157647609212476) as an image target which allows the software to recognise the origin of the virtual world and elements of to the real world via the devices screen.

# Screenshots #

## Camera view with the app ##

![15648528035_a066bd9193_z.jpg](https://bitbucket.org/repo/Apn65b/images/2407037811-15648528035_a066bd9193_z.jpg)

## Camera view without the app ##

![photo.JPG](https://bitbucket.org/repo/Apn65b/images/2827840611-photo.JPG)

## Vision ##

This is what we're aiming for in the end.

![toimisto+tiiliseina+varjot copy.png](https://bitbucket.org/repo/Apn65b/images/2474211813-toimisto+tiiliseina+varjot%20copy.png)